﻿using System;
using RestSharp;
using Newtonsoft.Json;
using WebAPIApplication.Common.Models;
using System.IO;
using System.Collections.Generic;

namespace RestTester
{
    class Program
    {
        private const string BaseUrl = "http://localhost:3010";

        static void Main(string[] args)
        {
            var client = new RestClient(BaseUrl);

            if (AskQuestion("Do you want to administrate search engine? y/n"))
            {
                Administrate(client);
            }
            else
            {
                Search(client);
            }
        }

        private static void Search(RestClient client)
        {
            var authModel = GetAuthModel();

            if (authModel == null)
            {
                return;
            }

            Search(client, authModel);
        }

        private static void Administrate(RestClient client)
        {
            var authModel = GetAuthModel(true);

            if (authModel == null)
            {
                return;
            }

            if (AskQuestion("Do you want to upload properties? y/n"))
            {
                UploadProperties(client, authModel);
            }

            if (AskQuestion("Do you want to upload managment companies? y/n"))
            {
                UploadCompanies(client, authModel);
            }

            if (AskQuestion("Do you want to search? y/n"))
            {
                Search(client, authModel);
            }
        }

        private static void UploadProperties(RestClient client, AuthModel authModel)
        {
            Upload<Property>(client, authModel, "properties");
        }

        private static void UploadCompanies(RestClient client, AuthModel authModel)
        {
            Upload<ManagCompany>(client, authModel, "companies");
        }

        private static void Upload<T>(RestClient client, AuthModel authModel, string name)
        {
            var entities = DeserializeWithRetry<T>(client, authModel, name);

            var requestPost = new RestRequest($"api/managment/{name}", Method.POST);
            requestPost.Timeout = 5 * 60 * 1000; //5 min
            requestPost.RequestFormat = DataFormat.Json;
            requestPost.AddHeader("authorization", $"{authModel.token_type} {authModel.access_token}");
            Console.WriteLine($"Trying to upload {name} to server...");
            requestPost.AddJsonBody(entities);
            var response = client.Post(requestPost);

            if (response.IsSuccessful)
                Console.WriteLine($"{name} upload completed successfully.");
            else
                Console.WriteLine($"{name} uploading error. {response.StatusCode} {response.ErrorMessage}");

            Console.ReadKey();
        }

        private static List<T> DeserializeWithRetry<T>(RestClient client, AuthModel authModel, string name)
        {
            Console.WriteLine($"Please, specify path to {name} containing json file.");
            var path = Console.ReadLine();

            var entities = new List<T>();
            var errorOccured = false;
            try
            {
                using (StreamReader r = new StreamReader(path))
                {
                    var json = r.ReadToEnd();
                    entities = JsonConvert.DeserializeObject<List<T>>(json);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occured when trying to upload {name}. {ex}");
                errorOccured = true;
            }

            if (errorOccured && AskQuestion("Do you want to retry uploading managment companies? y/n"))
            {
                return DeserializeWithRetry<T>(client, authModel, name);
            }

            return entities;
        }

        private static void Search(RestClient client, AuthModel authModel)
        {
            var first = true;

            while (first || AskQuestion("Do you want search again?"))
            {
                first = false;
                string query, market, limit;

                GetSearchParams(out query, out market, out limit);

                var limitInt = 25;
                int.TryParse(limit, out limitInt);

                DoSearch(client, authModel, query, market, limitInt);
            }
        }

        private static void DoSearch(RestClient client, AuthModel authModel, string query, string market, int limitInt)
        {
            var requestGet = new RestRequest("api/search", Method.GET);
            requestGet.AddHeader("authorization", $"{authModel.token_type} {authModel.access_token}");
            requestGet.AddQueryParameter("query", query);
            requestGet.AddQueryParameter("market", market);
            requestGet.AddQueryParameter("limit", limitInt.ToString());

            Console.WriteLine("Searching...");

            var response = client.Execute(requestGet);

            if (response.IsSuccessful)
            {
                Console.WriteLine("Search done. Results:");
                var results = JsonConvert.DeserializeObject<List<IndexItem>>(response.Content);
                results.ForEach(Console.WriteLine);
            }
            else
                Console.WriteLine($"Error in calling search engine. {response.StatusDescription}");
        }

        private static void GetSearchParams(out string query, out string market, out string limit)
        {
            Console.WriteLine("Enter string which you want to find...");
            query = Console.ReadLine();

            Console.WriteLine("Please, specify market or press enter...");
            market = Console.ReadLine();

            Console.WriteLine("Please, specify results count or press enter...");
            limit = Console.ReadLine();
        }

        private static AuthModel GetAuthModel(bool isAdmin = false)
        {
            Console.WriteLine($"Trying to get auth token...");

            var authClient = new RestClient("https://dev-qsvh6-3c.auth0.com/oauth/token");
            var authRequest = new RestRequest(Method.POST);
            authRequest.AddHeader("content-type", "application/json");

            AddAuthParameter(isAdmin, authRequest);

            var authResponse = authClient.Execute(authRequest);

            if (!authResponse.IsSuccessful)
            {
                Console.WriteLine($"Cannot authenticate client! {authResponse.StatusCode} {authResponse.Content}");
                return null;
            }

            var authModel = JsonConvert.DeserializeObject<AuthModel>(authResponse.Content);

            Console.WriteLine($"Authentication token got successefully. Press any key...");

            return authModel;
        }

        private static bool AskQuestion(string question)
        {
            Console.WriteLine(question);
            var answer = Console.ReadLine();
            return answer == "y";
        }

        private static void AddAuthParameter(bool isAdmin, RestRequest authRequest)
        {
            if (isAdmin)
            {
                authRequest.AddParameter("application/json", "{\"client_id\":\"cz75foG33UwX7wh8WI23XHoqmrmwPrun\",\"client_secret\":\"FcZ2u9FyqgkmvJ-uDe4eEGcPmw3dpJD1Edl7MIdX2FuFg-XXcsfmMVGdLmNddumR\",\"audience\":\"https://smartdatatest\",\"grant_type\":\"client_credentials\"}", ParameterType.RequestBody);
            }
            else
            {
                authRequest.AddParameter("application/json", "{\"client_id\":\"LyoxmhEpPxiUQthFPgqIG4nbJ9yeh69g\",\"client_secret\":\"oJRyLiBHl0UgoE74tNX0KgNRSrrqtGALVcQ__Va8W5OSrv_vjoxj91jBaB_-wlmn\",\"audience\":\"https://smartdatatest\",\"grant_type\":\"client_credentials\"}", ParameterType.RequestBody);
            }
        }
    }
}
