﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace WebAPIApplication.Common.Models
{
    public class ManagCompany
    {
        [Key]
        [JsonProperty("mgmtID")]
        public string MgmtID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("market")]
        public string Market { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }
}
