﻿using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using WebAPIApplication.Common.Enums;

namespace WebAPIApplication.Common.Models
{
    public class IndexItem
    {
        [Key]
        [IsFilterable]
        [JsonProperty("uid")]
        public string Uid { get; set; }

        [IsFilterable]
        [JsonProperty("originalId")]
        public string OriginalId { get; set; }

        [IsSearchable, IsSortable]
        [Analyzer(AnalyzerName.AsString.EnLucene)]
        [JsonProperty("name")]
        public string Name { get; set; }

        [IsSearchable, IsSortable]
        [Analyzer(AnalyzerName.AsString.EnLucene)]
        [JsonProperty("formerName")]
        public string FormerName { get; set; }

        [IsSearchable]
        [JsonProperty("streetAddress")]
        public string StreetAddress { get; set; }

        [IsSearchable, IsSortable]
        [JsonProperty("city")]
        public string City { get; set; }
        
        [IsSearchable, IsSortable, IsFilterable]
        [JsonProperty("market")]
        public string Market { get; set; }

        [IsSearchable, IsSortable]
        [JsonProperty("state")]
        public string State { get; set; }

        [IsFilterable, IsSortable]
        [JsonProperty("type")]
        public int Type { get; set; }

        [IsSortable]
        [JsonProperty("lat")]
        public double Lat { get; set; }

        [IsSortable]
        [JsonProperty("lng")]
        public double Lng { get; set; }

        public override string ToString()
        {
            var type = (EntityType)Type == EntityType.Property ? "Property" : "Managment company";
            return $"{type}  {OriginalId}{Environment.NewLine}Name: {Name}{Environment.NewLine}Market: {Market}{Environment.NewLine}State: {State}{Environment.NewLine}";
        }
    }
}
