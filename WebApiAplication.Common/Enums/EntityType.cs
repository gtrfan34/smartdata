﻿namespace WebAPIApplication.Common.Enums
{
    public enum EntityType
    {
        Property = 1,
        Company = 2
    }
}