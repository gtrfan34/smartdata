﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPIApplication.Common.Models;

namespace WebAPIApplication.Controllers
{
    [Route("api/search")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private ISearchEngine _searchEngine;

        public SearchController(ISearchEngine searchEngine)
        {
            _searchEngine = searchEngine;
        }

        [HttpGet]
        [Authorize("search")]
        public IEnumerable<IndexItem> Get([FromQuery] string query, [FromQuery] string market, [FromQuery] int limit)
        {
            var results = _searchEngine.Search(query, market, limit == 0 ? 25 : limit);

            return results;
        }
    }
}
