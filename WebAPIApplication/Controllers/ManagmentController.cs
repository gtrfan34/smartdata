﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPIApplication.Common.Models;
using WebAPIApplication.Services;

namespace WebAPIApplication.Controllers
{
    [Route("api/managment")]
    [ApiController]
    public class ManagmentController : ControllerBase
    {
        private IUploadManager _uploadManager;

        public ManagmentController(IUploadManager uploadManager)
        {
            _uploadManager = uploadManager;
        }

        [HttpPost]
        [Route("properties")]
        [Authorize("managment")]
        public void Post([FromBody] List<Property> properties)
        {
            _uploadManager.UploadProperties(properties);
        }

        [HttpPost]
        [Route("companies")]
        [Authorize("managment")]
        public void Post([FromBody] List<ManagCompany> companies)
        {
            _uploadManager.UploadCompanies(companies);
        }
    }
}
