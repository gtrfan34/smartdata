﻿using System.Collections.Generic;
using System.Linq;
using WebAPIApplication.Common.Enums;
using WebAPIApplication.Common.Models;

namespace WebAPIApplication.Converters
{
    public class IndexItemConverter
    {
        public const int companyIdOffset = 2000000000;

        public List<IndexItem> ConvertProperties(List<Property> properties)
        {
            var results = properties
                .Select(x => new IndexItem()
                {
                    Uid = x.PropertyId,
                    OriginalId = x.PropertyId,
                    Name = x.Name,
                    FormerName = x.FormerName,
                    City = x.City,
                    Market = x.Market,
                    State = x.State,
                    Type = (int)EntityType.Property,
                    StreetAddress = x.StreetAddress,
                    Lat = x.Lat,
                    Lng = x.Lng
                })
                .ToList();

            return results;
        }

        public List<IndexItem> ConvertCompanies(List<ManagCompany> companies)
        {
            var results = new List<IndexItem>(companies.Count);

            foreach (var comp in companies)
            {
                var uid = GetUniqueCompanyId(comp.MgmtID);
                if (uid == null)
                {
                    continue;
                }

                var indexItem = new IndexItem()
                {
                    Uid = uid,
                    OriginalId = comp.MgmtID,
                    Name = comp.Name,
                    Market = comp.Market,
                    State = comp.State,
                    Type = (int)EntityType.Company,
                };

                results.Add(indexItem);
            }
            
            return results;
        }

        /// <summary>
        /// Company Id added with companyIdOffset, to avoid conflicts inside one Azure index with properties
        /// </summary>
        private string GetUniqueCompanyId(string idString)
        {
            int oldId;
            if (!int.TryParse(idString, out oldId))
            {
                return null;
            }
            else
            {
                var newId = companyIdOffset + oldId;
                return newId.ToString();
            }
        }
    }
}
