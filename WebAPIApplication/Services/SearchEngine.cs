﻿using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using WebAPIApplication.Common.Models;

namespace WebAPIApplication.Services
{
    public class SearchEngine : ISearchEngine
    {
        private SearchIndexClient _serviceIndex;
        private IConfiguration _configuration;

        public SearchEngine(IConfiguration configuration)
        {
            _configuration = configuration;
            _serviceIndex = CreateSearchIndexClient();
        }

        public List<IndexItem> Search(string query, string market, int limit = 25)
        {
            var parameters =
                new SearchParameters()
                {
                    Select = new[] { "uid", "originalId", "name", "market", "state", "type" },
                    Top = limit
                };

            if (!string.IsNullOrEmpty(market))
            {
                parameters.Filter = $"market eq '{market}'";
            }

            var searchResults = _serviceIndex.Documents.Search<IndexItem>(query, parameters);

            var results = searchResults.Results
                .Select(x => x.Document)
                .ToList();

            return results;
        }

        private SearchIndexClient CreateSearchIndexClient()
        {
            var indexName = _configuration["Azure:SearchIndexName"];
            var searchServiceName = _configuration["Azure:SearchServiceName"];
            var queryApiKey = _configuration["Azure:SearchServiceQueryApiKey"];

            var indexClient = new SearchIndexClient(searchServiceName, indexName, new SearchCredentials(queryApiKey));
            return indexClient;
        }
    }
}