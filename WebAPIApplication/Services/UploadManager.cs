﻿using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using WebAPIApplication.Common.Models;
using WebAPIApplication.Converters;

namespace WebAPIApplication.Services
{
    public class UploadManager : IUploadManager
    {
        private IConfiguration _configuration;
        private SearchServiceClient serviceAdmin;

        public UploadManager(IConfiguration configuration)
        {
            _configuration = configuration;
            serviceAdmin = CreateSearchServiceClient();
        }

        public void UploadProperties(List<Property> properties)
        {
            var indexClient = PrepareIndex();

            var indexItemConverter = new IndexItemConverter();
            var indexItems = indexItemConverter.ConvertProperties(properties);

            UploadIndexItems(indexClient, indexItems);
        }

        public void UploadCompanies(List<ManagCompany> companies)
        {
            var indexClient = PrepareIndex();

            var indexItemConverter = new IndexItemConverter();
            var indexItems = indexItemConverter.ConvertCompanies(companies);

            UploadIndexItems(indexClient, indexItems);
        }

        private ISearchIndexClient PrepareIndex()
        {
            var indexName = _configuration["Azure:SearchIndexName"];
            CreateIndexIfNotExist();

            var indexClient = serviceAdmin.Indexes.GetClient(indexName);
            return indexClient;
        }

        private void UploadIndexItems(ISearchIndexClient indexClient, List<IndexItem> indexItems)
        {
            for (int i = 0; i < indexItems.Count; i += 1000)
            {
                var chunk = indexItems.Skip(i).Take(1000).ToArray();
                var batch = IndexBatch.Upload(chunk);
                try
                {
                    indexClient.Documents.Index(batch);
                }
                catch (IndexBatchException e)
                {
                    // Sometimes when your Search service is under load, indexing will fail for some of the documents in
                    // the batch. Depending on your application, you can take compensating actions like delaying and
                    // retrying. For this simple demo, we just log the failed document keys and continue.
                    Console.WriteLine(
                        "Failed to index some of the documents: {0}",
                        String.Join(", ", e.IndexingResults.Where(r => !r.Succeeded).Select(r => r.Key)));
                }
                Console.WriteLine("Waiting for documents to be indexed...\n");
                Thread.Sleep(2000);
            }
        }

        private SearchServiceClient CreateSearchServiceClient()
        {
            string searchServiceName = _configuration["Azure:SearchServiceName"];
            string adminApiKey = _configuration["Azure:SearchServiceAdminApiKey"];

            return new SearchServiceClient(searchServiceName, new SearchCredentials(adminApiKey));
        }

        private void CreateIndexIfNotExist()
        {
            var indexName = _configuration["Azure:SearchIndexName"];

            if (serviceAdmin.Indexes.Exists(indexName))
            {
                return;
            }

            var definition = new Index()
            {
                Name = indexName,
                Fields = FieldBuilder.BuildForType<IndexItem>(),
                ScoringProfiles = new List<ScoringProfile>()
                {
                    new ScoringProfile("relevance",
                        new TextWeights(
                            new Dictionary<string, double>()
                            {
                                { "name", 5 }
                            }))
                }
            };

            serviceAdmin.Indexes.Create(definition);
        }
    }
}
