﻿using System.Collections.Generic;
using WebAPIApplication.Common.Models;

namespace WebAPIApplication
{
    public interface IUploadManager
    {
        void UploadCompanies(List<ManagCompany> companies);
        void UploadProperties(List<Property> properties);
    }
}