﻿using System.Collections.Generic;
using WebAPIApplication.Common.Models;

namespace WebAPIApplication
{
    public interface ISearchEngine
    {
        List<IndexItem> Search(string query, string market, int limit);
    }
}